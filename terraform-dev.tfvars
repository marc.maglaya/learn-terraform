# subnet1_cidr_block = "10.0.40.0/24"
# vpc_cidr_block = "10.0.0.0/16"

## List
# cidr_blocks = [ "10.0.0.0/16", "10.0.10.0/24", "172.31.48.0/24" ]

## Object
cidr_blocks = [
    {cidr_block = "10.0.0.0/16", name = "dev-vpc"},
    {cidr_block = "10.0.10.0/24", name = "dev-subnet1"},
    {cidr_block = "172.31.48.0/24", name = "dev-subnet2"}
]