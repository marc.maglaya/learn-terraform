provider
1. for provider not with harshicorp, you need to explicitly declare the provider in providers.tf

    # linode = {
    #   source  = "linode/linode"
    #   # version = "..."
    # }

2. two options in destroying resources, note always use option 1
    1.  by removing the resource in main.tf
    2. terraform command [resourceType.resourceName]
    `terraform destroy -target aws_subnet.dev-subnet-2`

# variables
passing variables thru terminal input or
terraform apply -var "subnet_cidr_block=10.0.30.0/24" or 
create a terraform.tfvars
terraform apply -var-file terraform-dev.tfvars


# global environmental variable
## set it by using prefix TF_VAR_(var name)
export TF_VAR_avail_zone=eu="eu-west-1"
## reference in main.tf
variable avail_zone {}
  
resources "" "" {
    availability_zone = var.avail_zone
}