1. manipulate state only throught TF commands
2. always set up a shared remote state
3. use state locking
4. back up your state files
5. use 1 state per environment
6. host TF scripts in Git repo
7. Continuous Integration for TF code
8. Apply TF Only through CD Pipeline