
provider "aws" {
    region = "eu-west-1"
}

# variable "vpc_cidr_block" {
#     description = "vpc_cidr_block"
# } 

resource "aws_vpc" "development-vpc" {
    ## List
    # cidr_block = var.cidr_blocks[0]
    ## Object
    cidr_block = var.cidr_blocks[0].cidr_block
    tags = {
      "Name" = var.cidr_blocks[0].name
    }
}


#variable

# variable "subnet1_cidr_block" {
#     description = "subnet cidr block"
#     #default = "10.0.10.0/24" #makes the variable optional
# }

variable "cidr_blocks" {
    description = "cidr blocks"
    ## validating list
    # type = list(string)
    
    ## validating object
    type = list(object({
        cidr_block = string
        name = string
    }))

}


resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.development-vpc.id
    # cidr_block = var.cidr_blocks[1]
    cidr_block = var.cidr_blocks[1].cidr_block
    availability_zone = "eu-west-1a"
    tags = {
      "Name" = var.cidr_blocks[1].name
    }
}

#query

data "aws_vpc" "existing_vpc" {
    default = true
}

resource "aws_subnet" "dev-subnet-2" {
    vpc_id = data.aws_vpc.existing_vpc.id
    cidr_block = var.cidr_blocks[2].cidr_block
    availability_zone = "eu-west-1b"
    tags = {
      "Name" = var.cidr_blocks[2].name
    }
}

#output

output "dev-vpc-id" {
    value = aws_vpc.development-vpc.id
}

output "dev-subnet1-id" {
    value = aws_subnet.dev-subnet-1.id
}

output "dev-subnet2-id" {
    value = aws_subnet.dev-subnet-2.id
}

