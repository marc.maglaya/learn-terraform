##initialized the project
terraform init

#apply changes
terraform apply

#options auto approve
terraform apply -auto-approve

#dry-run
terraform plan

#clear-up
terraform destroy

#check resources state
terraform state list
terraform state show aws_subnet.dev-subnet-1

# passing variables value
terraform apply -var "subnet_cidr_block=10.0.30.0/24"
or 
terraform apply -var-file terraform-dev.tfvars

# searching for value of id of resource
terraform state show {resourceType.resourseName}